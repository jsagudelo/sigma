class Contact < ApplicationRecord
  validates  :nombre, :ciudad, length: {maximum: 50 }
  validates  :departamento, :correo,  length: {maximum: 30 }
end
