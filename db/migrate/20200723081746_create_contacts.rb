class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.text :nombre
      t.string :correo
      t.text :departamento
      t.text :ciudad

      t.timestamps
    end
  end
end
